DROP TABLE IF EXISTS account CASCADE;
DROP TABLE IF EXISTS ledger CASCADE;


-- Tables creation
CREATE TABLE IF NOT EXISTS account (
  accountID INT NOT NULL PRIMARY KEY,
  "name" TEXT,
  credit INT,
  bank_name text
);

CREATE TABLE IF NOT EXISTS ledger (
  ledgerID SERIAL NOT NULL PRIMARY KEY,
  fromID int NOT NULL REFERENCES account(accountID),
  toID int NOT NULL REFERENCES account(accountID),
  Fee int,
  Amount int,
  TransactionDateTime TIMESTAMP
);

-- Tables initialization
INSERT INTO account (accountID, "name", credit, bank_name)
VALUES 
  (0, 'Fees collector', 0, 'Tinkoff'),
  (1, 'Thomas Brady', 1000, 'SberBank'),
  (2, 'Adam Huff', 1000, 'Tinkoff'),
  (3, 'Theresa Pratt', 1000, 'SberBank');
-- Table's query