DROP TYPE IF EXISTS Account_Type CASCADE;
DROP FUNCTION IF EXISTS send_money CASCADE;
DROP FUNCTION IF EXISTS save_transaction CASCADE;

CREATE TYPE Account_Type AS (
    "name" TEXT,
    credit INT,
    bank_name text
);

-- Return data of account
CREATE OR REPLACE FUNCTION get_account(acc_id INT) RETURNS Account_Type AS $$
    DECLARE acc Account_Type;
BEGIN 
    SELECT "name", "credit", "bank_name" INTO acc
    FROM account
    WHERE accountID = acc_id
    LIMIT 1;
    RETURN acc;
EXCEPTION
    WHEN no_data_found THEN
        RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- insert transaction's data into table
CREATE or REPLACE FUNCTION save_transaction(from_id INT, to_id INT, amount INT, fee int) RETURNS VOID AS $$
BEGIN
    INSERT INTO ledger
        (fromID, toID, Fee, Amount, TransactionDateTime )
        VALUES 
            (from_id, to_id, fee, amount, NOW()::timestamp);
end;
$$ language plpgsql; 

-- transaction send_money
CREATE OR REPLACE FUNCTION send_money(from_id INT, to_id INT, amount INT) RETURNS VOID AS $$
    DECLARE acc_from Account_Type;
    DECLARE acc_to Account_Type;
    DECLARE fee INTEGER;
    DECLARE INTERNAL_FEE CONSTANT INTEGER DEFAULT 0;
    DECLARE EXTERNAL_FEE CONSTANT INTEGER DEFAULT 30;
BEGIN 
    SELECT * INTO acc_from
    FROM get_account(from_id);

    SELECT * INTO acc_to
    FROM get_account(to_id);

    IF from_id = to_id THEN
        RAISE EXCEPTION 'Incorrect account ID!';   
    END IF; 
    IF amount < 0 THEN
        RAISE EXCEPTION 'Incorrect amount value!';
    END IF;

    IF (acc_from).bank_name NOT LIKE (acc_to).bank_name THEN
        SELECT EXTERNAL_FEE INTO fee;
    ELSE
        SELECT INTERNAL_FEE INTO fee;
    END IF;

    IF (amount + fee) > (acc_from).credit
    THEN
        RAISE EXCEPTION 'Not enough money!';
    END IF;

    UPDATE account set credit = credit - amount - fee
    where accountID = from_id;

    UPDATE account set credit = credit + amount
    where accountID = to_id;

    UPDATE account set credit = credit + fee
    where accountID = 0;

    PERFORM save_transaction(from_id, to_id, amount, fee);

END;
$$ LANGUAGE plpgsql;    

