-- ---------------------------------------
-- EX1 

select * from account;
select * from Ledger;
-- ------------------------------

BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL SERIALIZABLE;
  select send_money(1,3,500);
  select send_money(2,1,700);
  select send_money(2,3,100);
  select * from account;
  select * from Ledger;

-- ------------------------------
ROLLBACK;
select * from account;
select * from Ledger;

-- ---------------------------------------