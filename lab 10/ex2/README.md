# EX 2

## SubTask 1

### Read Committed

1. Both terminals displayed **different** information, because **read committed** guarantee that only committed changes will be read. T1 shows the value of `jones`, while T2 shows `ajones`. After commit, both terminals show the same information.
2. At the eighth step, terminal T2 is blocked and waits for T1 to commit the transaction, since the same record is changed in both transactions.

### Repeatable Read

1. Both terminals displayed **different** information, because **repeatable read** guarantees that any data read cannot change, if the transaction reads the same data again. T1 shows the value of `jones`, while T2 shows `ajones`. After commit, both terminals still show the different information.
2. On second terminal, this session was canceled because the update affects a record already changed in another transaction but this isolation level does not allow this.

## SubTask 2

### Read Committed

We see that Mike got +15 to his balance, but bbrow did not. This is because bbrow was not yet in group 2 at the time the T1 transaction was processed because T2 did not commit the transaction.

### Repeatable Read

The same result, because T2 updated a record that was not read by T1.
