DROP TABLE IF EXISTS account CASCADE;
DROP TABLE IF EXISTS ledger CASCADE;

CREATE TABLE IF NOT EXISTS  account (
  id SERIAL NOT NULL PRIMARY KEY,
  username text,
  fullname text,
  balance int,
  Group_id int
);

INSERT INTO
    account (username, fullname, balance, Group_id)
VALUES
    ('jones',     'Alice Jones',      82,   1),
    ('bitdiddl',  'Ben Bitdiddle',    65,   1),
    ('mike',      'Michael Dole',     73,   2),
    ('alyssa',    'Alyssa P. Hacker', 79,   3),
    ('bbrown',    'Bob Brown',        100,  3);