--!--------------------------------------------------------------------------------------------------------------------------------------------------
--! task1_1 -- Read committed
--!--------------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------
--* Terminal 1
-- 1
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Read committed;
  select * from account;
-- 3
  select * from account;
-- 5+
  select * from account;
-- 7
  UPDATE account set balance = balance+10 where fullname = 'Alice Jones';
-- 9
  commit;
-- ---------------------------------------

-- ---------------------------------------
--* Terminal 2
-- 2
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Read committed;
  UPDATE account set username = 'ajones' where fullname = 'Alice Jones';
-- 4
  select * from account;
-- 5
  commit;
  select * from account;
-- 6 
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Read committed;
-- 8
  UPDATE account set balance = balance+20 where fullname = 'Alice Jones';
-- 10
  rollback;
-- ---------------------------------------

--!--------------------------------------------------------------------------------------------------------------------------------------------------
--! task1_2 -- Repeatable read
--!--------------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------
-- Terminal 1
-- 1
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Repeatable read;
  select * from account;
-- 3
  select * from account;
-- 5+
  select * from account;
-- 7
  UPDATE account set balance = balance+10 where fullname = 'Alice Jones';
-- 9
  commit;
-- ---------------------------------------

-- ---------------------------------------
-- Terminal 2
-- 2
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Repeatable read;
  UPDATE account set username = 'ajones' where fullname = 'Alice Jones';
-- 4
  select * from account;
-- 5
  commit;
  select * from account;
-- 6 
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Repeatable read;
-- 8
  UPDATE account set balance = balance+20 where fullname = 'Alice Jones';
-- 10
  rollback;
-- ---------------------------------------

--!--------------------------------------------------------------------------------------------------------------------------------------------------
--! task2_1 -- Read committed
--!--------------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------
-- Terminal 1
-- 1
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Read committed;
-- 3
  select * from account where group_id=2;
-- 5
  select * from account where group_id=2;
  
  UPDATE account
  SET balance = balance + 15
  where group_id=2;
-- 6
  commit;
-- ---------------------------------------

-- ---------------------------------------
-- Terminal 2
-- 2
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Read committed;
-- 4
  UPDATE account set group_id = 2 where fullname = 'Bob Brown';
-- 7
  commit;
-- ---------------------------------------

--!--------------------------------------------------------------------------------------------------------------------------------------------------
--! task2_1 -- Repeatable read
--!--------------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------
-- Terminal 1
-- 1
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Repeatable read;
-- 3
  select * from account where group_id=2;
-- 5
  select * from account where group_id=2;
  
  UPDATE account
  SET balance = balance + 15
  where group_id=2;
-- 6
  commit;
-- ---------------------------------------

-- ---------------------------------------
-- Terminal 2
-- 2
  BEGIN TRANSACTION;
  set transaction ISOLATION LEVEL Repeatable read;
-- 4
  UPDATE account set group_id = 2 where fullname = 'Bob Brown';
-- 7
  commit;
-- ---------------------------------------