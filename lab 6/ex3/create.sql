DROP TABLE IF EXISTS students CASCADE;
DROP TABLE IF EXISTS courses CASCADE;
DROP TABLE IF EXISTS registration CASCADE;

--Tables creation
CREATE TABLE IF NOT EXISTS students(
    "sid" INT NOT NULL PRIMARY KEY,
    sname CHAR(16) NOT NULL
);

CREATE TABLE IF NOT EXISTS courses(
    cid INT NOT NULL PRIMARY KEY,
    cname CHAR(16) NOT NULL
);

CREATE TABLE IF NOT EXISTS registration(
    "sid" INT NOT NULL,
    cid INT NOT NULL,
    percent REAL NOT NULL,
    --
    PRIMARY KEY("sid", cid),
    FOREIGN KEY ("sid") REFERENCES students ("sid"),
    FOREIGN KEY (cid) REFERENCES courses (cid)
);

INSERT INTO 
    students ("sid", sname)
VALUES
    (1, 'John McCarthy'),
    (2, 'Dennis Ritchie'),
    (3, 'Ken Thompson'),
    (4, 'Claude Shannon'),
    (5, 'Alan Turing'),
    (6, 'Alonzo Church'),
    (7, 'Perry White'),
    (8, 'Moshe Vardi'),
    (9, 'Roy Batty');

INSERT INTO
    courses (cid, cname)
VALUES
    (1, 'LISP'),
    (2, 'Unix'),
    (3, 'Info Theory'),
    (4, 'Turing Machines'),
    (5, 'Turing Test'),
    (6, 'Lambda Calculus');

INSERT INTO
    registration ("sid", cid, percent)
VALUES
    (1, 1, 10),
    (2, 2, 20),
    (3, 2, 30),
    (4, 3, 40),
    (5, 4, 50),
    (9, 5, 60),
    (6, 6, 70),
    (9, 1, 80),
    (6, 1, 90),
    (8, 3, 91),
    (1, 6, 92);
