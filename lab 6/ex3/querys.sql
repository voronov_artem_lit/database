-- Ex 3.1 Find the distinct names of all students who score more than 75% in the course numbered '1'
SELECT DISTINCT sname, percent
FROM students s
    INNER JOIN registration r ON s.sid = r.sid
WHERE percent > 75 AND cid = 1
ORDER BY percent;

-- Ex 3.2 Find the number of student whose score is 75% or above in each course
SELECT sname, AVG(percent) avg_grade
FROM students s
    INNER JOIN registration r ON s.sid = r.sid
GROUP BY s.sid
having MIN(percent) > 75
ORDER BY s.sid;

-- Ex 3.3 Find those students who are registered on no more than 2 courses
SELECT s.sid, sname, COUNT(*) num_courses 
FROM students s
    INNER JOIN registration r ON s.sid = r.sid
GROUP BY s.sid
having COUNT(*) <= 2 
ORDER BY s.sid;