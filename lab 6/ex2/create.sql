DROP TABLE IF EXISTS author CASCADE;
DROP TABLE IF EXISTS pub CASCADE;
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS author_pub CASCADE;

--Tables creation
CREATE TABLE IF NOT EXISTS author(
    author_id NUMERIC NOT NULL PRIMARY KEY,
    first_name text NOT NULL,
    last_name text NOT NULL
);

CREATE TABLE IF NOT EXISTS book(
    book_id text NOT NULL PRIMARY KEY,
    book_title text NOT NULL,
    "month" text,
    "year" int,
    editor int
);

CREATE TABLE IF NOT EXISTS pub(
    pub_id int NOT NULL PRIMARY KEY,
    title text NOT NULL,
    book_id text NOT NULL,
    --
    FOREIGN KEY (book_id) REFERENCES book(book_id)
);

CREATE TABLE author_pub(
    author_id int NOT NULL,
    pub_id int NOT NULL,
    author_position int,
    PRIMARY KEY(author_id, pub_id),
    --
    FOREIGN KEY (author_id) REFERENCES author(author_id),
    FOREIGN KEY (pub_id) REFERENCES pub(pub_id)
);

--Tables setup

INSERT INTO
    book (book_id, book_title, "month", "year", editor)
VALUES
    (1, 'CACM', 'April', 1960, 8),
    (2, 'CACM', 'July', 1974, 8),
    (3, 'BTS', 'July', 1948, 2),
    (4, 'MLS', 'November', 1936, 7),
    (5, 'Mind', 'October', 1950, NULL),
    (6, 'AMS', 'Month', 1941, NULL),
    (7, 'AAAI', 'July', 2012, 9),
    (8, 'NIPS', 'July', 2012, 9);

INSERT INTO
    author (author_id, first_name, last_name)
VALUES
    (1, 'John', 'McCarthy'),
    (2, 'Dennis', 'Ritchie'),
    (3, 'Ken', 'Thompson'),
    (4, 'Claude', 'Shannon'),
    (5, 'Alan', 'Turing'),
    (6, 'Alonzo', 'Church'),
    (7, 'Perry', 'White'),
    (8, 'Moshe', 'Vardi'),
    (9, 'Roy', 'Batty');

INSERT INTO
    pub (pub_id, title, book_id)
VALUES
    (1, 'LISP', 1),
    (2, 'Unix', 2),
    (3, 'Info Theory', 3),
    (4, 'Turing Machines', 4),
    (5, 'Turing Test', 5),
    (6, 'Lambda Calculus', 6);

INSERT INTO
    author_pub (author_id, pub_id, author_position)
VALUES
    (1, 1, 1),
    (2, 2, 1),
    (3, 2, 2),
    (4, 3, 1),
    (5, 4, 1),
    (5, 5, 1),
    (6, 6, 1);