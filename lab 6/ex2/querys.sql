-- Ex 2.1 𝐴𝑢𝑡ℎ𝑜𝑟 ⋈ 𝑎𝑢𝑡ℎ𝑜𝑟𝑖𝑑=𝑒𝑑𝑖𝑡𝑜𝑟 𝐵𝑜𝑜𝑘
-- Authors who have written a book
SELECT *
FROM author, book
WHERE author_id = editor;

-- Ex 2.2 Π 𝑓𝑖𝑟𝑠𝑡_𝑛𝑎𝑚𝑒,𝑙𝑎𝑠𝑡_𝑛𝑎𝑚𝑒 (( Π 𝑎𝑢𝑡ℎ𝑜𝑟_𝑖𝑑 (𝐴𝑢𝑡ℎ𝑜𝑟) − Π 𝑒𝑑𝑖𝑡𝑜𝑟 (𝐵𝑜𝑜𝑘) ⋈ 𝐴𝑢𝑡ℎ𝑜𝑟))
-- Authors who have not written a book
SELECT first_name, last_name
FROM (
        SELECT author_id FROM author
        EXCEPT
        SELECT editor FROM book
    ) AS ID 
    INNER JOIN author a on ID.author_id = a.author_id;

-- Ex 2.3 Π 𝑎𝑢𝑡ℎ𝑜𝑟_𝑖𝑑 (𝐴𝑢𝑡ℎ𝑜𝑟) − Π 𝑒𝑑𝑖𝑡𝑜𝑟 (𝑏𝑜𝑜𝑘)
-- author_id of Authors who have not written a book
SELECT author_id FROM author
EXCEPT
SELECT editor FROM book;