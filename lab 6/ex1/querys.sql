-- 1) Find the names of suppliers who supply some red part
SELECT sname
FROM Catalog
    INNER JOIN Parts ON Catalog.pid = Parts.pid
    INNER JOIN Suppliers ON Catalog.sid = Suppliers.sid
WHERE Parts.color = 'Red'
GROUP BY sname;

-- 2) Find the sids of suppliers who supply some red or green part
SELECT DISTINCT sid
FROM Catalog
    INNER JOIN Parts p ON Catalog.pid = p.pid
WHERE p.color IN ('Red', 'Green');

-- 3) Find the sids of suppliers who supply some red part or are at 221 Packer Street
(
    SELECT DISTINCT sid
    FROM Catalog
        INNER JOIN Parts ON Catalog.pid = Parts.pid
    WHERE Parts.color = 'Red'
)
UNION
(
    SELECT DISTINCT sid
    FROM Suppliers
    WHERE address LIKE '%221%Packer%'
);


-- 4) Find the sids of suppliers who supply some red part and some green part
(
    SELECT DISTINCT sid
    FROM Catalog
        INNER JOIN Parts ON Catalog.pid = Parts.pid
    WHERE Parts.color = 'Red'
)
INTERSECT
(
    SELECT DISTINCT sid
    FROM Catalog
        INNER JOIN Parts ON Catalog.pid = Parts.pid
    WHERE Parts.color = 'Green'
);


-- 5) Find the sids of suppliers who supply every part
SELECT sid
FROM Catalog
GROUP BY sid
having count(*) = (
        SELECT count(*) cnt
        FROM parts
    );


-- 6) Find the sids of suppliers who supply every red part
SELECT sid
FROM Catalog c INNER JOIN Parts p ON c.pid = p.pid
WHERE p.color = 'Red'
GROUP BY sid
having count(*) = (
        SELECT count(*) cnt
        FROM parts
        WHERE color = 'Red'
    );



-- 7) Find the sids of suppliers who supply every red and every green part
SELECT sid
FROM Catalog c INNER JOIN Parts p ON c.pid = p.pid
WHERE p.color in ('Red','Green')
GROUP BY sid
having count(*) = (
        SELECT count(*) cnt
        FROM parts
        WHERE color in ('Red','Green')
    );

-- 8) Find the sids of suppliers who supply every red part or supply every green part:
SELECT sid
FROM Catalog c INNER JOIN Parts p ON c.pid = p.pid
WHERE p.color = 'Red'
GROUP BY sid
having count(*) = (
        SELECT count(*) cnt
        FROM parts
        WHERE color = 'Red'
    )
UNION
SELECT sid
FROM Catalog c INNER JOIN Parts p ON c.pid = p.pid
WHERE p.color = 'Green'
GROUP BY sid
having count(*) = (
        SELECT count(*) cnt
        FROM parts
        WHERE color = 'Green'
    );

-- 9) Find pairs of sids such that the supplier with the first sid charges more for some part than the supplier with the second sid
SELECT  A.sid AS sid_A, A.cost AS cost_A, B.sid AS sid_B, B.cost AS cost_B, A.pid
FROM Catalog AS A, Catalog AS B
WHERE A.pid = B.pid AND A.cost > B.cost;

-- 10) Find the pids of parts supplied by at least two different suppliers
SELECT  DISTINCT A.pid, A.sid AS sid_A, B.sid AS sid_B
FROM Catalog AS A, Catalog AS B
WHERE A.pid = B.pid AND A.sid != B.sid;

-- 11) find the average cost of the red parts and green parts for each of the suppliers
SELECT 
  c.sid, 
  p.color,
  AVG(c.cost) avg_cost
FROM Catalog c INNER JOIN Parts p ON c.pid = p.pid
WHERE p.color IN ('Green', 'Red')
GROUP BY c.sid, p.color;

-- 12) find the sids of suppliers whose most expensive part costs $50 or more
SELECT 
  c.sid, 
  MAX(c.cost) cost_cost
FROM Catalog AS c
WHERE c.cost >= 50
GROUP BY c.sid;

