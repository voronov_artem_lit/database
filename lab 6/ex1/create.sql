DROP TABLE IF EXISTS Suppliers CASCADE;
DROP TABLE IF EXISTS Parts CASCADE;
DROP TABLE IF EXISTS Catalog CASCADE;
-- 
CREATE TABLE IF NOT EXISTS Suppliers (
    sid INT NOT NULL,
    sname TEXT,
    address TEXT,
    -- 
    PRIMARY KEY (sid)
);
CREATE TABLE IF NOT EXISTS Parts (
    pid INT NOT NULL,
    pname TEXT,
    color TEXT,
    -- 
    PRIMARY KEY (pid)
);
CREATE TABLE IF NOT EXISTS Catalog (
    pid INT,
    sid INT,
    cost REAL,
    -- 
    PRIMARY KEY (pid, sid),
    FOREIGN KEY (sid) REFERENCES Suppliers(sid),
    FOREIGN KEY (pid) REFERENCES Parts(pid)
);
-- 
-- 
INSERT INTO suppliers (sid, sname, address)
VALUES (1, 'Yosemite Sham', 'Devil''s canyon, AZ'),
    (2, 'Wiley E.Coyote', 'RR Asylum, NV'),
    (4, 'Wiley E.Coyote', '221 Packer Street'),
    (3, 'Elmer Fudd', 'Carrot Patch, MN');
    
INSERT INTO parts (pid, pname, color)
VALUES (1, 'Red1', 'Red'),
    (2, 'Red2', 'Red'),
    (3, 'Green1', 'Green'),
    (4, 'Blue1', 'Blue'),
    (5, 'Red3', 'Red');

INSERT INTO catalog (sid, pid, cost)
VALUES (1, 1, 10),
    (1, 2, 20),
    (1, 3, 30),
    (1, 4, 40),
    (1, 5, 50),
    (2, 1, 9),
    -- (2, 2, 23),
    (2, 3, 34),
    (2, 5, 48),
    (3, 3, 12);