
-- Ex 1: Calculate the total number of items per order and the total amount to pay for the order.
SELECT o.id orderId, SUM(oi.quantity) quant, SUM(oi.quantity * i.price) amount 
FROM orderitem oi, item i, ordert o, customer c
WHERE oi.oid = o.id AND oi.iid = i.id AND o.cid = c.id
GROUP BY o.id
ORDER BY o.id;

-- Ex 2: Obtain the customer whose purchase in terms of money has been greater than the others.
SELECT c.name
FROM orderitem oi, item i, ordert o, customer c
WHERE oi.oid = o.id AND oi.iid = i.id AND o.cid = c.id 
GROUP BY c.id
HAVING SUM(oi.quantity * i.price) >= ALL(
    SELECT SUM(oi.quantity * i.price) amount
    FROM orderitem oi, item i, ordert o, customer c
    WHERE oi.oid = o.id AND oi.iid = i.id AND o.cid = c.id
    GROUP BY c.id
);
