DROP TABLE IF EXISTS Customer CASCADE;
DROP TABLE IF EXISTS Item CASCADE;
DROP TABLE IF EXISTS OrderT CASCADE;
DROP TABLE IF EXISTS OrderItem CASCADE;

-- Tables creation
CREATE TABLE IF NOT EXISTS Customer (
    id int NOT NULL,
    "name" char(32) NOT NULL,
    city char(32),
    ---
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Item (
    id int NOT NULL,
    "name" char(32) NOT NULL,
    price float NOT NULL,
    ---
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS OrderT (
    id int NOT NULL,
    "date" date,
    cId int NOT NULL,
    ---
    PRIMARY KEY (id),
    FOREIGN KEY (cId) REFERENCES Customer (id)
);

CREATE TABLE IF NOT EXISTS OrderItem (
    oId int NOT NULL,
    iId int NOT NULL,
    quantity int DEFAULT 1,
    ---
    PRIMARY KEY (oId, iId),
    FOREIGN KEY (oId) REFERENCES OrderT(id),
    FOREIGN KEY (iId) REFERENCES Item (id)
);

-- Tables initialization
INSERT INTO
    Customer (id, "name", city)
VALUES
    (101, 'Martin', 'Prague'),
    (107, 'Herman', 'Madrid'),
    (110, 'Pedoro', 'Moscow');

INSERT INTO
    OrderT (id, "date", cId)
VALUES
    (2301, '23/02/2011', 101),
    (2302, '25/02/2011', 107),
    (2303, '27/02/2011', 110);

INSERT INTO
    Item (id, "name", price)
VALUES
    (3786, 'Net', 35.0),
    (4011, 'Racket', 65.0),
    (9132, 'Pack-3', 4.75),
    (5794, 'Pack-6', 5.0),
    (3141, 'Cover', 10.0);

INSERT INTO
    OrderItem (oId, iId, quantity)
VALUES
    (2301, 3786, 3),
    (2301, 4011, 6),
    (2301, 9132, 8),
    (2302, 5794, 4),
    (2303, 4011, 2),
    (2303, 3141, 2);

-- Table's query

SELECT o.id orderId, "date", c.id customerId, c.name customerName, c.city, i.id itemId, i.name itemName, oi.quantity quant, (oi.quantity * i.price) price 
FROM orderitem oi, item i, ordert o, customer c
WHERE oi.oid = o.id AND oi.iid = i.id AND o.cid = c.id
ORDER BY o.id, c.id, i.id;