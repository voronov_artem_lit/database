-- Original table query
SELECT s.name school,
  t.name teacher,
  c.title course,
  r.name room,
  g.name grade,
  b.title book,
  p.name publisher,
  l.dateL loanDate
FROM 
    book b, course c, grade g, loan l, publisher p, school s, teacher t, teacherGradeCourse tgc, room r, teacherRoom tr
WHERE p.id = b.pid
  AND b.id = l.bid
  AND s.id = t.sid
  AND r.id = tr.rid
  AND t.id = tr.tid
  AND tgc.id = l.tgcid
  AND t.id = tgc.tid
  AND g.id = tgc.gid
  AND c.id = tgc.cid
ORDER BY l.id, s.id, t.id, c.id, r.id, b.id;

-- Ex 1: Obtain for each of the schools, the number of books that have been loaned to each publishers.
SELECT s.name school, p.name publishers, count(*) "total books"
FROM  book b, course c, grade g, loan l, publisher p, school s, teacher t, teacherGradeCourse tgc, room r, teacherRoom tr
WHERE p.id = b.pid AND b.id = l.bid AND s.id = t.sid AND r.id = tr.rid AND t.id = tr.tid AND tgc.id = l.tgcid AND t.id = tgc.tid AND g.id = tgc.gid AND c.id = tgc.cid
GROUP BY s.id, p.id;

-- Ex 2: For each school, find the book that has been on loan the longest and the teacher in charge of it
SELECT s1.name school, l1.dateL "date of loan", b1.title "longest book", t1.name "owner"
FROM  book b1, course c1, grade g1, loan l1, publisher p1, school s1, teacher t1, teacherGradeCourse tgc1, room r1, teacherRoom tr1
WHERE p1.id = b1.pid AND b1.id = l1.bid AND s1.id = t1.sid AND r1.id = tr1.rid AND t1.id = tr1.tid AND tgc1.id = l1.tgcid AND t1.id = tgc1.tid AND g1.id = tgc1.gid AND c1.id = tgc1.cid
    AND l1.dateL <= ALL(
        SELECT l2.dateL
        FROM  book b2, course c2, grade g2, loan l2, publisher p2, school s2, teacher t2, teacherGradeCourse tgc2, room r2, teacherRoom tr2
        WHERE p2.id = b2.pid AND b2.id = l2.bid AND s2.id = t2.sid AND r2.id = tr2.rid AND t2.id = tr2.tid AND tgc2.id = l2.tgcid AND t2.id = tgc2.tid AND g2.id = tgc2.gid AND c2.id = tgc2.cid
            AND s1.id = s2.id
);