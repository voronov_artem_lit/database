DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS course CASCADE;
DROP TABLE IF EXISTS grade CASCADE;
DROP TABLE IF EXISTS loan CASCADE;
DROP TABLE IF EXISTS publisher CASCADE;
DROP TABLE IF EXISTS school CASCADE;
DROP TABLE IF EXISTS teacher CASCADE;
DROP TABLE IF EXISTS teacherGradeCourse CASCADE;
DROP TABLE IF EXISTS room CASCADE;
DROP TABLE IF EXISTS teacherRoom CASCADE;

-- Tables creation

        
CREATE TABLE book
(
  id    INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  title char(128) NOT NULL,
  pid   INT       NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE course
(
  id    INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  title char(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE grade
(
  id   INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  name char(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE loan
(
  id    INT  NOT NULL GENERATED ALWAYS AS IDENTITY,
  bid   INT  NOT NULL,
  dateL date NOT NULL,
  tgcid INT  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE publisher
(
  id   INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  name char(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE room
(
  id   INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  name char(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE school
(
  id   INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  name char(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE teacher
(
  id   INT       NOT NULL GENERATED ALWAYS AS IDENTITY,
  name char(128) NOT NULL,
  sid  INT       NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE teacherGradeCourse
(
  id  INT NOT NULL GENERATED ALWAYS AS IDENTITY,
  tid INT NOT NULL,
  gid INT NOT NULL,
  cid INT NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE teacherRoom
(
  tid INT NOT NULL,
  rid INT NOT NULL,
  PRIMARY KEY (tid, rid)
);

ALTER TABLE book
  ADD CONSTRAINT FK_publisher_TO_book
    FOREIGN KEY (pid)
    REFERENCES publisher (id);

ALTER TABLE teacher
  ADD CONSTRAINT FK_school_TO_teacher
    FOREIGN KEY (sid)
    REFERENCES school (id);

ALTER TABLE teacherGradeCourse
  ADD CONSTRAINT FK_teacher_TO_teacherGradeCourse
    FOREIGN KEY (tid)
    REFERENCES teacher (id);

ALTER TABLE teacherGradeCourse
  ADD CONSTRAINT FK_grade_TO_teacherGradeCourse
    FOREIGN KEY (gid)
    REFERENCES grade (id);

ALTER TABLE teacherRoom
  ADD CONSTRAINT FK_teacher_TO_teacherRoom
    FOREIGN KEY (tid)
    REFERENCES teacher (id);

ALTER TABLE teacherRoom
  ADD CONSTRAINT FK_room_TO_teacherRoom
    FOREIGN KEY (rid)
    REFERENCES room (id);

ALTER TABLE teacherGradeCourse
  ADD CONSTRAINT FK_course_TO_teacherGradeCourse
    FOREIGN KEY (cid)
    REFERENCES course (id);

ALTER TABLE loan
  ADD CONSTRAINT FK_book_TO_loan
    FOREIGN KEY (bid)
    REFERENCES book (id);

ALTER TABLE loan
  ADD CONSTRAINT FK_teacherGradeCourse_TO_loan
    FOREIGN KEY (tgcid)
    REFERENCES teacherGradeCourse (id);

        
      
-- Tables initialization
INSERT INTO 
    publisher(name)
VALUES
    ('BOA Editions'),
    ('Taylor & Francis Publishing'),
    ('Prentice Hall'),
    ('McGraw Hill');

INSERT INTO
    book(title, pid)
VALUES
    ('Learning and teaching in early childhood', 1),
    ('Preschool,N56', 2),
    ('Early Childhood Education N9', 3),
    ('Know how to educate: guide for Parents and ...', 4);

INSERT INTO
    grade(name)
VALUES
    ('1st grade'),
    ('2nd grade');

INSERT INTO
    course(title)
VALUES
    ('Logical thinking'),
    ('Wrtting'),
    ('Numerical Thinking'),
    ('Spatial, Temporal and Causal Thinking'),
    ('English');

INSERT INTO
    room(name)
VALUES
    ('1.A01'),
    ('1.B01'),
    ('2.B01');
  
INSERT INTO
    school(name)
VALUES
    ('Horizon Education Institute'),
    ('Bright Institution');

INSERT INTO
    teacher(name, sid)
VALUES
    ('Chad Russell', 1),
    ('E.F.Codd',     1),
    ('Jones Smith',  1),
    ('Adam Baker',   2);
    
INSERT INTO
    teacherRoom(tid,rid)
VALUES
    (1,1),
    (2,2),
    (3,1),
    (4,3);

INSERT INTO
    teacherGradeCourse(tid,gid,cid)
VALUES
    (1,1,1),
    (1,1,2),
    (1,1,3),
    (2,1,4),
    (2,1,3),
    (3,2,2),
    (3,2,5),
    (4,1,1),
    (4,1,3);

INSERT INTO
    loan(tgcid, bid, dateL)
VALUES
    (1, 1,'09/09/2010'),
    (2, 2,'05/05/2010'),
    (3, 1,'05/05/2010'),
    (4, 3,'06/05/2010'),
    (5, 1,'06/05/2010'),
    (6, 1,'09/09/2010'),
    (7, 4,'05/05/2010'),
    (8, 4,'18/12/2010'),
    (9, 1,'06/05/2010');




