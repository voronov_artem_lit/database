CREATE TABLE GroupT
(
  groupid NUMERIC NOT NULL,
  PRIMARY KEY (groupid)
);

CREATE TABLE Company
(
  companyid NUMERIC NOT NULL,
  groupid INT NOT NULL,
  structure_companyid INT NOT NULL,
  PRIMARY KEY (companyid),
  FOREIGN KEY (groupid) REFERENCES GroupT(groupid),
  FOREIGN KEY (structure_companyid) REFERENCES Company(companyid)
);

CREATE TABLE Plant
(
  plantid NUMERIC NOT NULL,
  companyid INT NOT NULL,
  PRIMARY KEY (plantid),
  FOREIGN KEY (companyid) REFERENCES Company(companyid)
);

CREATE TABLE Item
(
  itemid NUMERIC NOT NULL,
  plantid INT NOT NULL,
  PRIMARY KEY (itemid),
  FOREIGN KEY (plantid) REFERENCES Plant(plantid)
);