CREATE TABLE Airport
(
  IATACode INT NOT NULL,
  PRIMARY KEY (IATACode)
);

CREATE TABLE Flight
(
  flightNum INT NOT NULL,
  PRIMARY KEY (flightNum)
);

CREATE TABLE DailyFlightLegCombination
(
  DFLegId INT NOT NULL,
  PRIMARY KEY (DFLegId)
);

CREATE TABLE FlightLeg
(
  flightLegId INT NOT NULL,
  start_airport INT NOT NULL,
  finish_airport INT NOT NULL,
  DFLegId INT,
  flightNum INT NOT NULL,
  PRIMARY KEY (flightLegId),
  FOREIGN KEY (start_airport) REFERENCES Airport(IATACode),
  FOREIGN KEY (finish_airport) REFERENCES Airport(IATACode),
  FOREIGN KEY (DFLegId) REFERENCES DailyFlightLegCombination(DFLegId),
  FOREIGN KEY (flightNum) REFERENCES Flight(flightNum)
);

CREATE TABLE AircraftType
(
  typeId INT NOT NULL,
  DFLegId INT,
  PRIMARY KEY (typeId),
  FOREIGN KEY (DFLegId) REFERENCES DailyFlightLegCombination(DFLegId)
);

CREATE TABLE can_land
(
  IATACode INT NOT NULL,
  typeId INT NOT NULL,
  PRIMARY KEY (IATACode, typeId),
  FOREIGN KEY (IATACode) REFERENCES Airport(IATACode),
  FOREIGN KEY (typeId) REFERENCES AircraftType(typeId),
  UNIQUE (IATACode, typeId)
);