CREATE TABLE Airport (iata_code serial PRIMARY KEY);

CREATE TABLE AircraftType (type_id serial PRIMARY KEY);

CREATE TABLE AirportsAircraftTypes (
    airport_id INT NOT NULL FOREIGN KEY REFERENCES Airport(iata_code),
    aircrafttype_id INT NOT NULL FOREIGN KEY REFERENCES AircraftType(type_id),
    PRIMARY KEY (airport_id, aircrafttype_id)
);

CREATE TABLE FlightLeg (
    flightleg_id serial PRIMARY KEY,
    start_airport INT NOT NULL FOREIGN KEY REFERENCES Airport(iata_code),
    finish_airport INT NOT NULL FOREIGN KEY REFERENCES Airport(iata_code)
);

CREATE TABLE Flight (
    flight_num serial PRIMARY KEY,
    flightleg_id INT NOT NULL FOREIGN KEY REFERENCES FlightLeg(flightleg_id)
);

CREATE TABLE DailyFlightLegCombination (
    dfleg_id serial PRIMARY KEY,
    flightleg_id INT NOT NULL FOREIGN KEY REFERENCES FlightLeg(flightleg_id),
    aircrafttype_id INT NOT NULL FOREIGN KEY REFERENCES AircraftType(type_id)
);