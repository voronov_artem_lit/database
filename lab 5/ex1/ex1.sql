CREATE TABLE OrderT (
    orderId INT NOT NULL,
    date_a DATE NOT NULL,
    house# INT NOT NULL,
    street CHAR(255) NOT NULL,
    district CHAR(127) NOT NULL,
    sity CHAR(127) NOT NULL,
    PRIMARY KEY (orderId)
);

CREATE TABLE Item (
    itemId INT NOT NULL,
    description CHAR(255),
    PRIMARY KEY (itemId)
);

CREATE TABLE Manufacturer (
    manufacturerId INT NOT NULL,
    phonenumber CHAR(15),
    PRIMARY KEY (manufacturerId)
);

CREATE TABLE produce (
    quantity INT NOT NULL,
    manufacturerId INT NOT NULL,
    itemId INT NOT NULL,
    PRIMARY KEY (manufacturerId, itemId),
    FOREIGN KEY (manufacturerId) REFERENCES Manufacturer(manufacturerId),
    FOREIGN KEY (itemId) REFERENCES Item(itemId)
);

CREATE TABLE Includes (
    quantity INT NOT NULL,
    itemId INT NOT NULL,
    orderId INT NOT NULL,
    PRIMARY KEY (itemId, orderId),
    FOREIGN KEY (itemId) REFERENCES Item(itemId),
    FOREIGN KEY (orderId) REFERENCES OrderT(orderId)
);

CREATE TABLE Customer (
    clientId NUMERIC NOT NULL,
    balance FLOAT NOT NULL,
    creditLimit INT NOT NULL,
    discount INT,
    house# INT NOT NULL,
    street CHAR(255) NOT NULL,
    district CHAR(127) NOT NULL,
    sity CHAR(127) NOT NULL,
    orderId INT NOT NULL,
    PRIMARY KEY (clientId),
    FOREIGN KEY (orderId) REFERENCES OrderT(orderId)
);