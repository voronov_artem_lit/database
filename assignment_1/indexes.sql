-- Indexes for Query 1 (auditing staff):
-- SELECT r1.staff_id, p1.payment_date
-- FROM rental r1, payment p1
-- WHERE r1.rental_id = p1.rental_id AND
-- NOT EXISTS (SELECT 1 FROM rental r2, customer c WHERE r2.customer_id =
-- c.customer_id and active = 1 and r2.last_update > r1.last_update);

-- opclass: ASC, DESC, NULLS FIRST, NULLS LAST

-- Customer: customer_id, ~active=1~
-- Hash
    create index on customer using hash(customer_id);
    create index on customer using hash(active);
    create index on customer using hash(customer_id) where active = 1;
    create index on customer using hash(active) where active = 1;
-- Btree
    create index on customer(customer_id)
        with (fillfactor =100);
    create index on customer(active)
        with (fillfactor =100);
    create index on customer(customer_id,active)
        with (fillfactor =100);
    -- ADD include to All prev
    create index on customer(customer_id)
        include (active)
        with (fillfactor =100);
    create index on customer(active)
        include (customer_id)
        with (fillfactor =100);
    -- ADD Where to ALL prev
    create index on customer(customer_id)
        with (fillfactor =100)
        where active = 1;
    create index on customer(active)
        with (fillfactor =100)
        where active = 1;
    create index on customer(customer_id,active)
        with (fillfactor =100)
        where active = 1;
    --
    create index on customer(customer_id)
        include (active)
        with (fillfactor =100)
        where active = 1;
    create index on customer(active)
        include (customer_id)
        with (fillfactor =100)
        where active = 1;
-- Payment: rental_id, !payment_date!
-- Hash
    create index on payment using hash(rental_id);
    create index on payment using hash(payment_date);
-- Btree
    create index on payment(rental_id)
        with (fillfactor =100);
    create index on payment(payment_date)
        with (fillfactor =100);
    create index on payment(rental_id, payment_date)
        with (fillfactor =100);
    -- ADD include to All prev
    create index on payment(rental_id)
        include (payment_date)
        with (fillfactor =100);
    create index on payment(payment_date)
        include (rental_id)
        with (fillfactor =100);
-- Rental: rental_id, customer_id, last_update, !staff_id!
-- Hash
create index on rental using hash(rental_id);
create index on rental using hash(customer_id);
create index on rental using hash(last_update);
create index on rental using hash(staff_id);
-- One
create index on rental(rental_id)
    with (fillfactor =100);
create index on rental(customer_id)
    with (fillfactor =100);
create index on rental(last_update)
    with (fillfactor =100);
create index on rental(staff_id)
    with (fillfactor =100);
-- Two
create index on rental(rental_id, customer_id)
    with (fillfactor =100);
create index on rental(rental_id, last_update)
    with (fillfactor =100);
create index on rental(rental_id, staff_id)
    with (fillfactor =100);
create index on rental(customer_id, last_update)
    with (fillfactor =100);
create index on rental(customer_id, staff_id)
    with (fillfactor =100);
create index on rental(last_update, staff_id)
    with (fillfactor =100);
-- Three
create index on rental(customer_id, last_update, staff_id)
    with (fillfactor =100);
create index on rental(rental_id, last_update, staff_id)
    with (fillfactor =100);
create index on rental(rental_id, customer_id, staff_id)
    with (fillfactor =100);
create index on rental(rental_id, customer_id, last_update)
    with (fillfactor =100);
-- Four
create index on rental(rental_id, customer_id, last_update, staff_id)
    with (fillfactor =100);

-- INCLUDE staff_id ONE
create index on rental(rental_id)
    include (staff_id)
    with (fillfactor =100);
create index on rental(customer_id)
    include (staff_id)
    with (fillfactor =100);
create index on rental(last_update)
    include (staff_id)
    with (fillfactor =100);
-- INCLUDE staff_id Two
create index on rental(rental_id, customer_id)
    include (staff_id)
    with (fillfactor =100);
create index on rental(rental_id, last_update)
    include (staff_id)
    with (fillfactor =100);
create index on rental(customer_id, last_update)
    include (staff_id)
    with (fillfactor =100);
-- INCLUDE staff_id Three
create index on rental(rental_id, customer_id, last_update)
    include (staff_id)
    with (fillfactor =100);

-- INCLUDE last_update ONE
create index on rental(rental_id)
    include (last_update)
    with (fillfactor =100);
create index on rental(customer_id)
    include (last_update)
    with (fillfactor =100);
create index on rental(staff_id)
    include (last_update)
    with (fillfactor =100);
-- INCLUDE last_update Two
create index on rental(rental_id, customer_id)
    include (last_update)
    with (fillfactor =100);
create index on rental(rental_id, staff_id)
    include (last_update)
    with (fillfactor =100);
create index on rental(customer_id, staff_id)
    include (last_update)
    with (fillfactor =100);
-- INCLUDE last_update Three
create index on rental(rental_id, customer_id, staff_id)
    include (last_update)
    with (fillfactor =100);

-- INCLUDE customer_id ONE
create index on rental(rental_id)
    include (customer_id)
    with (fillfactor =100);
create index on rental(last_update)
    include (customer_id)
    with (fillfactor =100);
create index on rental(staff_id)
    include (customer_id)
    with (fillfactor =100);
-- INCLUDE customer_id Two
create index on rental(rental_id, last_update)
    include (customer_id)
    with (fillfactor =100);
create index on rental(rental_id, staff_id)
    include (customer_id)
    with (fillfactor =100);
create index on rental(last_update, staff_id)
    include (customer_id)
    with (fillfactor =100);
-- INCLUDE customer_id Three
create index on rental(rental_id, last_update, staff_id)
    include (customer_id)
    with (fillfactor =100);

-- INCLUDE rental_id ONE
create index on rental(customer_id)
    include (rental_id)
    with (fillfactor =100);
create index on rental(last_update)
    include (rental_id)
    with (fillfactor =100);
create index on rental(staff_id)
    include (rental_id)
    with (fillfactor =100);
-- INCLUDE rental_id Two
create index on rental(customer_id, last_update)
    include (rental_id)
    with (fillfactor =100);
create index on rental(customer_id, staff_id)
    include (rental_id)
    with (fillfactor =100);
create index on rental(last_update, staff_id)
    include (rental_id)
    with (fillfactor =100);
-- INCLUDE rental_id Three
create index on rental(customer_id, last_update, staff_id)
    include (rental_id)
    with (fillfactor =100);

-- INCLUDE rental_id, customer_id 
create index on rental(last_update)
    include (rental_id, customer_id)
    with (fillfactor =100);
create index on rental(staff_id)
    include (rental_id, customer_id)
    with (fillfactor =100);
create index on rental(last_update, staff_id)
    include (rental_id, customer_id)
    with (fillfactor =100);

-- INCLUDE rental_id, last_update
create index on rental(customer_id)
    include (rental_id, last_update)
    with (fillfactor =100);
create index on rental(staff_id)
    include (rental_id, last_update)
    with (fillfactor =100);
create index on rental(customer_id, staff_id)
    include (rental_id, last_update)
    with (fillfactor =100);

-- INCLUDE rental_id, staff_id
create index on rental(customer_id)
    include (rental_id, staff_id)
    with (fillfactor =100);
create index on rental(last_update)
    include (rental_id, staff_id)
    with (fillfactor =100);
create index on rental(customer_id, last_update)
    include (rental_id, staff_id)
    with (fillfactor =100);

-- INCLUDE customer_id, last_update
create index on rental(rental_id)
    include (customer_id, last_update)
    with (fillfactor =100);
create index on rental(staff_id)
    include (customer_id, last_update)
    with (fillfactor =100);
create index on rental(rental_id, staff_id)
    include (customer_id, last_update)
    with (fillfactor =100);

-- INCLUDE customer_id, staff_id
create index on rental(rental_id)
    include (customer_id, staff_id)
    with (fillfactor =100);
create index on rental(last_update)
    include (customer_id, staff_id)
    with (fillfactor =100);
create index on rental(rental_id, last_update)
    include (customer_id, staff_id)
    with (fillfactor =100);

-- INCLUDE last_update, staff_id
create index on rental(rental_id)
    include (last_update, staff_id)
    with (fillfactor =100);
create index on rental(customer_id)
    include (last_update, staff_id)
    with (fillfactor =100);
create index on rental(rental_id, customer_id)
    include (last_update, staff_id)
    with (fillfactor =100);

-- include(customer_id, last_update, staff_id)
create index on rental(rental_id)
    include(customer_id, last_update, staff_id)
    with (fillfactor =100);
-- include(rental_id, last_update, staff_id)
create index on rental(customer_id)
    include(rental_id, last_update, staff_id)
    with (fillfactor =100);
-- include(rental_id, customer_id, staff_id)
create index on rental(last_update)
    include(rental_id, customer_id, staff_id)
    with (fillfactor =100);
-- include(rental_id, customer_id, last_update)
create index on rental(staff_id)
    include(rental_id, customer_id, last_update)
    with (fillfactor =100);


create index on rental(rental_id, last_update)
    include (staff_id)
    with (fillfactor =100);
    
CREATE INDEX ON public.rental USING btree(last_update)
 INCLUDE (customer_id)
 WITH (fillfactor='100');


-- create index on rental (last_update) WITH (FILLFACTOR = 100);

-- create index q1_customer_5 on rental (last_update) WITH (FILLFACTOR = 100);


-- --Indexes for Query 2 (popular movies year by year):
-- EXPLAIN ANALYZE
-- SELECT title, release_year
-- FROM film f1
-- WHERE f1.rental_rate > (SELECT AVG(f2.rental_rate) FROM film f2 WHERE
-- f1.release_year = f2.release_year);
