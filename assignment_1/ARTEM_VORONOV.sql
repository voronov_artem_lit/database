--! table_name 1:
-- SELECT r1.staff_id, p1.payment_date
-- FROM rental r1, payment p1
-- WHERE r1.rental_id = p1.rental_id
--     AND NOT EXISTS (
--         SELECT 1
--         FROM rental r2, customer c
--         WHERE r2.customer_id = c.customer_id
--             and active = 1
--             and r2.last_update > r1.last_update
--     );


CREATE INDEX q1_1 ON payment(rental_id) INCLUDE (payment_date) WITH (fillfactor = 27);
CREATE INDEX q1_2 ON rental(customer_id, last_update) WITH (fillfactor = 100);
CREATE INDEX q1_3 ON rental(last_update, customer_id) WITH (fillfactor = 100);
CREATE INDEX q1_4 ON customer(customer_id) INCLUDE (active) WITH (fillfactor = 100);
CREATE INDEX q1_5 ON rental(last_update) INCLUDE(customer_id) WITH (fillfactor = 100);
CREATE INDEX q1_6 ON rental(rental_id) INCLUDE(last_update, staff_id) WITH (fillfactor = 20);



--! table_name 2:
-- SELECT title, release_year
-- FROM film f1
-- WHERE f1.rental_rate > (
--         SELECT AVG(f2.rental_rate)
--         FROM film f2
--         WHERE f1.release_year = f2.release_year
--     );

CREATE INDEX q2_1 ON film(release_year) INCLUDE(title, rental_rate) WITH (fillfactor = 100);

--! table_name 3:
-- SELECT f.title, f.release_year,
--     (
--         SELECT SUM(p.amount)
--         FROM payment p, rental r1, inventory i1
--         WHERE p.rental_id = r1.rental_id
--             AND r1.inventory_id = i1.inventory_id
--             AND i1.film_id = f.film_id
--     )
-- FROM film f
-- WHERE NOT EXISTS (
--         SELECT c.first_name, count(*)
--         FROM customer c, rental r2, inventory i1, film f1, film_actor fa, actor a
--         WHERE c.customer_id = r2.customer_id
--             AND r2.inventory_id = i1.inventory_id
--             AND i1.film_id = f1.film_id
--             and f1.rating in ('PG-13', 'NC-17')
--             AND f1.film_id = fa.film_id
--             AND f1.film_id = f.film_id
--             AND fa.actor_id = a.actor_id
--             and a.first_name = c.first_name
--         GROUP BY c.first_name
--         HAVING count(*) > 2
--     );


CREATE INDEX q3_1 ON customer using hash(first_name) WITH (fillfactor = 100);
CREATE INDEX q3_2 ON rental(inventory_id) INCLUDE(rental_id) WITH (fillfactor = 100);
CREATE INDEX q3_3 ON film_actor(film_id) INCLUDE(actor_id) WITH (fillfactor = 100);
CREATE INDEX q3_4 ON rental(customer_id) INCLUDE(inventory_id) WITH (fillfactor = 100);
CREATE INDEX q3_5 ON inventory using hash(inventory_id) WITH (fillfactor = 100);
CREATE INDEX q3_6 ON inventory using hash(film_id) WITH (fillfactor = 100);
CREATE INDEX q3_7 ON inventory(film_id) INCLUDE(inventory_id) WITH (fillfactor = 100);
CREATE INDEX q3_8 ON payment using hash(rental_id) WITH (fillfactor = 100);
CREATE INDEX q3_9 ON film using hash(film_id) WITH (fillfactor = 100) WHERE rating IN ('PG-13', 'NC-17');

