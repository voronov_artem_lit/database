

-- Query 1 (auditing staff): 
--* before: 2_284_549.49
--* after:  1_557_074.21
--* percent: 
EXPLAIN ANALYZE
SELECT r1.staff_id, p1.payment_date
FROM rental r1, payment p1
WHERE r1.rental_id = p1.rental_id
    AND NOT EXISTS (
        SELECT 1
        FROM rental r2, customer c
        WHERE r2.customer_id = c.customer_id
            and active = 1
            and r2.last_update > r1.last_update
    );

-- Nested Loop Anti Join  (cost=535.06..1557074.21 rows=9731 width=10) (actual time=525.157..561.074 rows=1 loops=1)
--   ->  Hash Join  (cost=510.99..803.28 rows=14596 width=18) (actual time=509.741..522.243 rows=14596 loops=1)
--         Hash Cond: (p1.rental_id = r1.rental_id)
--         ->  Seq Scan on payment p1  (cost=0.00..253.96 rows=14596 width=12) (actual time=0.041..2.613 rows=14596 loops=1)
--         ->  Hash  (cost=310.44..310.44 rows=16044 width=14) (actual time=509.379..509.380 rows=16044 loops=1)
--               Buckets: 16384  Batches: 1  Memory Usage: 881kB
--               ->  Seq Scan on rental r1  (cost=0.00..310.44 rows=16044 width=14) (actual time=504.284..506.371 rows=16044 loops=1)
--   ->  Hash Join  (cost=24.07..131.85 rows=5214 width=8) (actual time=0.002..0.002 rows=1 loops=14596)
--         Hash Cond: (r2.customer_id = c.customer_id)
--         ->  Index Scan using indx_1_1 on rental r2  (cost=0.29..93.92 rows=5348 width=10) (actual time=0.002..0.002 rows=1 loops=14596)
--               Index Cond: (last_update > r1.last_update)
--         ->  Hash  (cost=16.49..16.49 rows=584 width=4) (actual time=0.288..0.288 rows=584 loops=1)
--               Buckets: 1024  Batches: 1  Memory Usage: 29kB
--               ->  Seq Scan on customer c  (cost=0.00..16.49 rows=584 width=4) (actual time=0.033..0.143 rows=584 loops=1)
--                     Filter: (active = 1)
--                     Rows Removed by Filter: 15
-- Planning Time: 3.571 ms
-- JIT:
--   Functions: 27
--   Options: Inlining true, Optimization true, Expressions true, Deforming true
--   Timing: Generation 8.311 ms, Inlining 137.833 ms, Optimization 229.867 ms, Emission 136.138 ms, Total 512.148 ms
-- Execution Time: 628.435 ms



-- Query 2 (popular movies year by year): 
--* before: 69_079.00 
--* after: 
--* percent: 
EXPLAIN ANALYZE
SELECT title, release_year
FROM film f1
WHERE f1.rental_rate > (
        SELECT AVG(f2.rental_rate)
        FROM film f2
        WHERE f1.release_year = f2.release_year
    );
-- Seq Scan on film f1  (cost=0.00..69079.00 rows=333 width=19) (actual time=0.382..283.097 rows=659 loops=1)
--   Filter: (rental_rate > (SubPlan 1))
--   Rows Removed by Filter: 341
--   SubPlan 1
--     ->  Aggregate  (cost=69.00..69.01 rows=1 width=32) (actual time=0.282..0.282 rows=1 loops=1000)
--           ->  Seq Scan on film f2  (cost=0.00..66.50 rows=1000 width=6) (actual time=0.001..0.147 rows=1000 loops=1000)
--                 Filter: ((f1.release_year)::integer = (release_year)::integer)
-- Planning Time: 0.362 ms
-- Execution Time: 283.214 ms
;



-- Query 3 (how much movies [which have not been rented by teenagers who rented the
-- movie because there are actors with the same name as them] earn for the store):
--* before: 293_114.54
--* after: 98851.13
--* percent: 
EXPLAIN ANALYZE
SELECT f.title, f.release_year,
    (
        SELECT SUM(p.amount)
        FROM payment p, rental r1, inventory i1
        WHERE p.rental_id = r1.rental_id
            AND r1.inventory_id = i1.inventory_id
            AND i1.film_id = f.film_id
    )
FROM film f
WHERE NOT EXISTS (
        SELECT c.first_name, count(*)
        FROM customer c, rental r2, inventory i1, film f1, film_actor fa, actor a
        WHERE c.customer_id = r2.customer_id
            AND r2.inventory_id = i1.inventory_id
            AND i1.film_id = f1.film_id
            and f1.rating in ('PG-13', 'NC-17')
            AND f1.film_id = fa.film_id
            AND f1.film_id = f.film_id
            AND fa.actor_id = a.actor_id
            and a.first_name = c.first_name
        GROUP BY c.first_name
        HAVING count(*) > 2
    );

	
-- Seq Scan on film f  (cost=0.00..98851.13 rows=500 width=51) (actual time=75.460..963.479 rows=1000 loops=1)
--   Filter: (NOT (SubPlan 2))
--   SubPlan 1
--     ->  Aggregate  (cost=180.84..180.85 rows=1 width=32) (actual time=0.461..0.461 rows=1 loops=1000)
--           ->  Nested Loop  (cost=4.60..180.79 rows=16 width=6) (actual time=0.191..0.456 rows=15 loops=1000)
--                 ->  Nested Loop  (cost=4.32..174.49 rows=18 width=4) (actual time=0.187..0.415 rows=16 loops=1000)
--                       ->  Seq Scan on inventory i1  (cost=0.00..82.26 rows=5 width=4) (actual time=0.178..0.375 rows=5 loops=1000)
--                             Filter: (film_id = f.film_id)
--                             Rows Removed by Filter: 4576
--                       ->  Bitmap Heap Scan on rental r1  (cost=4.32..18.41 rows=4 width=8) (actual time=0.004..0.006 rows=4 loops=4581)
--                             Recheck Cond: (inventory_id = i1.inventory_id)
--                             Heap Blocks: exact=16036
--                             ->  Bitmap Index Scan on idx_fk_inventory_id  (cost=0.00..4.32 rows=4 width=0) (actual time=0.002..0.002 rows=4 loops=4581)
--                                   Index Cond: (inventory_id = i1.inventory_id)
--                 ->  Index Scan using idx_fk_rental_id on payment p  (cost=0.29..0.34 rows=1 width=10) (actual time=0.002..0.002 rows=1 loops=16044)
--                       Index Cond: (rental_id = r1.rental_id)
--   SubPlan 2
--     ->  GroupAggregate  (cost=101.64..101.67 rows=1 width=14) (actual time=0.427..0.427 rows=0 loops=1000)
--           Group Key: c.first_name
--           Filter: (count(*) > 2)
--           Rows Removed by Filter: 0
--           ->  Sort  (cost=101.64..101.65 rows=1 width=6) (actual time=0.426..0.426 rows=0 loops=1000)
--                 Sort Key: c.first_name
--                 Sort Method: quicksort  Memory: 25kB
--                 ->  Nested Loop  (cost=23.48..101.63 rows=1 width=6) (actual time=0.417..0.423 rows=0 loops=1000)
--                       ->  Nested Loop  (cost=23.20..93.33 rows=1 width=10) (actual time=0.407..0.423 rows=0 loops=1000)
--                             ->  Nested Loop  (cost=22.92..51.13 rows=135 width=12) (actual time=0.108..0.266 rows=81 loops=1000)
--                                   ->  Hash Join  (cost=22.92..40.21 rows=5 width=12) (actual time=0.103..0.201 rows=3 loops=1000)
--                                         Hash Cond: ((c.first_name)::text = (a.first_name)::text)
--                                         ->  Seq Scan on customer c  (cost=0.00..14.99 rows=599 width=10) (actual time=0.002..0.057 rows=599 loops=997)
--                                         ->  Hash  (cost=22.86..22.86 rows=5 width=8) (actual time=0.058..0.058 rows=5 loops=1000)
--                                               Buckets: 1024  Batches: 1  Memory Usage: 9kB
--                                               ->  Hash Join  (cost=18.32..22.86 rows=5 width=8) (actual time=0.025..0.054 rows=5 loops=1000)
--                                                     Hash Cond: (a.actor_id = fa.actor_id)
--                                                     ->  Seq Scan on actor a  (cost=0.00..4.00 rows=200 width=10) (actual time=0.003..0.019 rows=200 loops=997)
--                                                     ->  Hash  (cost=18.26..18.26 rows=5 width=4) (actual time=0.013..0.013 rows=5 loops=1000)
--                                                           Buckets: 1024  Batches: 1  Memory Usage: 9kB
--                                                           ->  Bitmap Heap Scan on film_actor fa  (cost=4.32..18.26 rows=5 width=4) (actual time=0.006..0.010 rows=5 loops=1000)
--                                                                 Recheck Cond: (film_id = f.film_id)
--                                                                 Heap Blocks: exact=5075
--                                                                 ->  Bitmap Index Scan on idx_fk_film_id  (cost=0.00..4.32 rows=5 width=0) (actual time=0.004..0.004 rows=5 loops=1000)
--                                                                       Index Cond: (film_id = f.film_id)
--                                   ->  Index Scan using indx_1_0 on rental r2  (cost=0.00..1.91 rows=27 width=6) (actual time=0.002..0.018 rows=27 loops=2986)
--                                         Index Cond: (customer_id = c.customer_id)
--                             ->  Index Scan using inventory_pkey on inventory i1_1  (cost=0.28..0.31 rows=1 width=6) (actual time=0.002..0.002 rows=0 loops=81255)
--                                   Index Cond: (inventory_id = r2.inventory_id)
--                                   Filter: (film_id = f.film_id)
--                                   Rows Removed by Filter: 1
--                       ->  Index Scan using film_pkey on film f1  (cost=0.28..8.29 rows=1 width=4) (actual time=0.004..0.004 rows=0 loops=65)
--                             Index Cond: (film_id = f.film_id)
--                             Filter: (rating = ANY ('{PG-13,NC-17}'::mpaa_rating[]))
--                             Rows Removed by Filter: 1
-- Planning Time: 4.460 ms
-- JIT:
--   Functions: 68
--   Options: Inlining false, Optimization false, Expressions true, Deforming true
--   Timing: Generation 10.351 ms, Inlining 0.000 ms, Optimization 3.744 ms, Emission 67.903 ms, Total 81.998 ms
-- Execution Time: 974.346 ms

