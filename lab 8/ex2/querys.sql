-- Q1: The company is preparing its campaign for next Halloween, so the list of movies that have
-- not been rented yet by the clients is needed, whose rating is R or PG-13 and its category is
-- Horror or Sci-fi
EXPLAIN ANALYZE SELECT DISTINCT ON (f.film_id) f.title, f.film_id, i.inventory_id
FROM film_category fc
    INNER JOIN film f ON fc.film_id = f.film_id
    INNER JOIN category c ON fc.category_id = c.category_id
    INNER JOIN inventory i ON f.film_id = i.film_id
WHERE rating IN ('R','PG-13') AND c.name IN ('Horror','Sci-fi')
      AND  i.inventory_id NOT IN (
          SELECT inventory_id
          FROM rental
      )
ORDER BY f.film_id
 -- The most expensive step of this queries is:
    --> Seq Scan on rental (cost=0.00..310.44 rows=16044 width=4) 
 -- We can avoid this by reorganizing the query so that comparisons occur in indexes.
;
    
-- Q2: The company has decided to reward the best stores in each of the cities, so it is necessary
-- to have a list of the stores that have made a greater number of sales in term of money
-- during the last month recorded.


EXPLAIN ANALYZE SELECT distinct ON (ls.city_id) ls.*
FROM (SELECT c.city_id, c.city, s.store_id, sum(p.amount) AS amount
      FROM store s
        INNER JOIN staff sff ON s.store_id = sff.store_id
        INNER JOIN payment p ON sff.staff_id = p.staff_id
        INNER JOIN "address" a ON s.address_id = a.address_id
        INNER JOIN city c ON a.city_id = c.city_id
      GROUP BY c.city_id, s.store_id
     ) ls
ORDER BY ls.city_id
 -- The most expensive step of this queries is:
    --> Nested Loop (cost=6.17..215.63 rows=14596 width=138) for Group Key: c.city_id, s.store_id
 -- We can avoid this by reorganizing the query
;