
# postgresql import(restore) database from tar
pg_restore -c -U vorart -d dvdrental -v "/home/vorart/workspace/subjects/database/lab 8/ex2/dvdrental.tar" -W

# -c to clean the database
# -U to force a user
# -d to select the database
# -v verbose mode, don't know why
# "$$" the location of the files to import in tmp to get around permission issues
# -W to force asking for the password to the user (postgres)