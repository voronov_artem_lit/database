import psycopg2
from faker import Faker
# https://stackabuse.com/working-with-postgresql-in-python/
con = psycopg2.connect(database="lab8_ex1", user="vorart",
                       password="password", host="127.0.0.1", port="5432")

print("Database opened successfully")
cur = con.cursor()

# cur.execute('''select 'drop table "' || tablename || '" cascade;' from pg_tables where schemaname = 'public';''')
# rows = cur.fetchall()
# for row in rows:
#     cur.execute(row[0])
#     print(row[0])
# con.commit()

cur.execute('''
    CREATE TABLE IF NOT EXISTS CUSTOMER (
        ID      INT PRIMARY KEY NOT NULL,
        Name    TEXT NOT NULL,
        Address TEXT NOT NULL,
        review  TEXT
    );''')
print("Table created successfully")
fake = Faker()
for i in range(100000):
    if i%1000==0: print(i)
    cur.execute("INSERT INTO CUSTOMER (ID,Name,Address,review) VALUES ('"+ str(i)+"','"+fake.name()+"','"+fake.address()+"','"+fake.text()+"')")
    con.commit()
 

#explain select * from customer
