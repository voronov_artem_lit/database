DROP INDEX IF EXISTS length_review CASCADE;
DROP INDEX IF EXISTS name_idx CASCADE;
DROP INDEX IF EXISTS name_like CASCADE;

-- Q0: select all from CUSTOMER
EXPLAIN ANALYZE SELECT * FROM CUSTOMER;
-- original cost: 4032

-- -- Q1: display all names that occur more than 30 times
EXPLAIN ANALYZE SELECT name, count(*) count
FROM customer
GROUP BY name
HAVING count(*) > 30 
ORDER BY -count(*);
-- -- original cost: 15765

-- Q2: display number of Smiths
EXPLAIN ANALYZE SELECT count(*)
FROM customer
WHERE name like 'John Smith';
-- original cost: 4282

-- Q3: display pair of people with same name that pirst person's review is bigger than second
EXPLAIN ANALYZE SELECT DISTINCT ON (name) name, "first id", "second id", lengthA, lengthB
FROM (
    SELECT A.name, A.id "first id", B.id "second id", length(A.review) lengthA, length(B.review) lengthB
    FROM customer A, customer B
    WHERE A.id != B.id AND A.name = B.name AND length(A.review) > length(B.review)
    LIMIT 10000000
) p;
-- original cost: 36971

CREATE INDEX name_idx  ON customer USING btree (name);
-- CREATE INDEX name_idx  ON customer(name);
CREATE INDEX name_like  ON customer USING hash (name);
-- CREATE INDEX name_like  ON customer(name);
CREATE INDEX length_review ON customer USING btree (length(review));
-- CREATE INDEX length_review ON customer(length(review));

-- Q0: select all from CUSTOMER
EXPLAIN ANALYZE SELECT * FROM CUSTOMER;
-- after index cost: 4032

-- Q1: display all names that occur more than 30 times
EXPLAIN ANALYZE SELECT name, count(*) count
FROM customer
GROUP BY name
HAVING count(*) > 30 
ORDER BY -count(*);
-- after index cost: 15765

-- Q2: display number of Smiths
EXPLAIN ANALYZE SELECT count(*)
FROM customer
WHERE name like 'John Smith';
-- after index cost: 141 -> 29 times faster

-- Q3: display pair of people with same name that pirst person's review is bigger than second
EXPLAIN ANALYZE SELECT DISTINCT ON (name) name, "first id", "second id", lengthA, lengthB
FROM (
    SELECT A.name, A.id "first id", B.id "second id", length(A.review) lengthA, length(B.review) lengthB
    FROM customer A, customer B
    WHERE A.id != B.id AND A.name = B.name AND length(A.review) > length(B.review)
    LIMIT 10000000
) p;
-- after index cost: 33921 (a little less)

SELECT
    tablename,
    indexname,
    indexdef
FROM
    pg_indexes
WHERE
    schemaname = 'public'
ORDER BY
    tablename,
    indexname;