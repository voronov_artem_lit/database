import datetime
from pymongo import MongoClient
from sqlalchemy import null
client = MongoClient('localhost', 27017)
db = client['lab11']

#?ex1
def ex1():
    print('EX1:')

    cursor = list(db.restaurants.find({'cuisine':'Indian'}))
    print('1_1:\n', cursor[0] if len(cursor) > 0 else 'null', f'\n...[{len(cursor)}]\n')

    cursor = list(db.restaurants.find({ 'cuisine': {'$in': ['Indian', 'Thai']} }))
    print('1_2:\n', cursor[0] if len(cursor) > 0 else 'null', f'\n...[{len(cursor)}]\n')

    cursor = list(db.restaurants.find({ 'address.building': '1115', 'address.street': 'Rogers Avenue', 'address.zipcode': '11226'}))
    print('1_3:\n', cursor[0] if len(cursor) > 0 else 'null', f'\n...[{len(cursor)}]\n')



#?ex2
def ex2():
    '''
    Address: 1480 2 Avenue, 10075, -73.9557413, 40.7720266
        • Borough: Manhattan
        • Cuisine: Italian
        • Name: Vella
        • Id: 41704620
        • Grades:
        • A, 11, 01 Oct, 2014
    '''
    print('EX2:')

    print('before insert:', db.restaurants.count_documents({'restaurant_id': '41704620'}))
    db.restaurants.insert_one({
        'address': {'building': '1480',
                    'coord': [-73.9557413, 40.7720266],
                    'street': '2 Avenue',
                    'zipcode': '10075'},
        'borough': 'Manhattan',
        'cuisine': 'Italian',
        'grades':  [{'date': datetime.datetime(2014, 10, 1), 'grade': 'A', 'score': 11}],
        'name': 'Vella',
        'restaurant_id': '41704620'
    })
    print('after:',db.restaurants.count_documents({'restaurant_id': '41704620'}), '\n')

#?ex3
def ex3():
    print('EX3:')

    print('Delete from the database a single Manhattan located restaurant:')
    print('\tbefore delete:', db.restaurants.count_documents({'borough': 'Manhattan'}))
    db.restaurants.find_one_and_delete({'borough': 'Manhattan'})
    print('\tafter:', db.restaurants.count_documents({'borough': 'Manhattan'}))

    result = db.restaurants.delete_many({'cuisine': 'Thai'})
    print('Delete from the database all Thai cuisines, deleted:', result.deleted_count, '\n')

#?ex4
def ex4():
    print('EX4:')
    deletedrow = 0
    updatedrow = 0

    cursor = db.restaurants.find({'address.street': 'Rogers Avenue'})
    for doc in cursor:
        count = 0
        for it in doc['grades']:
            if it['grade'] == 'C': count+=1
        if count > 1:
            # print('del: ' ,doc['_id']) 
            db.restaurants.delete_one({'_id': doc['_id']})
            deletedrow += 1
            continue
        doc['grades'].append({'date': datetime.datetime.now(), 'grade': 'C', 'score': 777})
        # print('upd: ' ,doc['_id']) 
        db.restaurants.update_one({'_id': doc['_id']}, {'$set': {'grades': doc['grades']}})
        updatedrow += 1
    
    print("Deleted rows", deletedrow)
    print("Updated rows", updatedrow)


ex1()
ex2()
ex3()
ex4()